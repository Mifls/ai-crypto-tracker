import { createApp } from 'vue'
import { createWebHistory, createRouter } from "vue-router";
import Toast from 'vue-toastification'
import 'vue-toastification/dist/index.css'
import App from './App.vue'

const routes = [];

const router = createRouter({
    history: createWebHistory(),
    routes
});

const app = createApp(App);
app.use(router);
app.use(Toast)
app.mount('#app')

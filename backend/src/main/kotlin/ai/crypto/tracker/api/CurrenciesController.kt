package ai.crypto.tracker.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.json.JSONException
import org.json.JSONArray
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ResponseStatusException
import java.net.URI


@Tag(name = "Currencies")
@RestController
@RequestMapping("/currencies")
class CurrenciesController {
    @Operation(summary = "Get list of currencies")
    @GetMapping
    fun listCurrencies(): String {
        val url = "https://api.coingecko.com/api/v3/coins/" +
                "markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false"
        val response = URI.create(url).toURL().readText(
            charset = Charsets.UTF_8
        )

        try {
            val jsonObject = JSONArray(response)
            return jsonObject.toString()
        } catch (err: JSONException) {
            throw ResponseStatusException(
                HttpStatus.SERVICE_UNAVAILABLE, "Service not available"
            )
        }
    }

}
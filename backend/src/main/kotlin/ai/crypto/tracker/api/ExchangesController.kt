package ai.crypto.tracker.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import java.net.URI
import org.json.JSONArray
import org.json.JSONException
import org.springframework.web.server.ResponseStatusException


@Tag(name = "Exchanges")
@RestController
@RequestMapping("/exchanges")
class ExchangesController {
    @Operation(summary = "Get list of exchanges")
    @GetMapping
    fun listExchanges(): String {
        val url = "https://api.coingecko.com/api/v3/exchanges"
        val response = URI.create(url).toURL().readText(
            charset = Charsets.UTF_8
        )

        try {
            val jsonObject = JSONArray(response)
            return jsonObject.toString()
        } catch (err: JSONException) {
            throw ResponseStatusException(
                HttpStatus.SERVICE_UNAVAILABLE, "Service not available"
            )
        }
    }


}
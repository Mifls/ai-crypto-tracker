package ai.crypto.tracker.api

import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping

import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import java.io.IOException

@Tag(name = "Portfolio")
@RestController
@RequestMapping("/portfolio")
class PortfolioController {

    // Create a single instance of OkHttpClient to be reused
    private val client = OkHttpClient()

    @PostMapping("/get_portfolio")
    fun getPortfolio(@RequestBody address: String): ResponseEntity<String> {
        val url = "https://grimace-watch-20de4c2cc559.herokuapp.com/get-data/"
        val mediaType = "application/json".toMediaType()

        val payload = """{"address": "${address}"}"""
        val requestBody = payload.toRequestBody(mediaType)

        val request = Request.Builder()
            .url(url)
            .post(requestBody)
            .addHeader("Content-Type", "application/json")
            .build()

        try {
            val response = client.newCall(request).execute()

            if (response.isSuccessful) {
                return ResponseEntity.ok(response.body?.string())
            } else {
                throw ResponseStatusException(
                    HttpStatus.SERVICE_UNAVAILABLE, "Service not available"
                )
            }
        } catch (e: IOException) {
            throw ResponseStatusException(
                HttpStatus.SERVICE_UNAVAILABLE, "Service not available", e
            )
        }
    }
}
package ai.crypto.tracker.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.tags.Tag
import org.json.JSONObject
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import oshi.SystemInfo
import oshi.hardware.CentralProcessor
import oshi.hardware.HardwareAbstractionLayer


@Tag(name = "Governor", description = "System speed management")
@RestController
@RequestMapping("/governor")
class GovernorController {
    @Operation(summary = "Get system information")
    @GetMapping("/system")
    fun listExchanges(): String {
        val systemInfo = SystemInfo()
        val hardware: HardwareAbstractionLayer = systemInfo.hardware
        val processor: CentralProcessor = hardware.processor
        val globalMemory = hardware.memory
        val operatingSystem = systemInfo.operatingSystem

        val processorIdentifier: CentralProcessor.ProcessorIdentifier = processor.processorIdentifier

        return JSONObject()
            .put("vendor", processorIdentifier.vendor)
            .put("freq", processorIdentifier.vendorFreq.toString())
            .put("ram", globalMemory.total.toString())
            .put("arch", System.getProperty("os.arch"))
//            .put("os", System.getProperty("os.name"))
            .put("version", System.getProperty("os.version"))
            .put("os", operatingSystem.family)
            .toString()
    }


}
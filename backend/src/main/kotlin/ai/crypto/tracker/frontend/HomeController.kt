package ai.crypto.tracker.frontend

import org.apache.logging.log4j.LogManager
import org.springframework.context.annotation.Configuration
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@RestController
@RequestMapping("/")
class HomeController {
    @Configuration
    inner class StaticResourceConfiguration : WebMvcConfigurer {
        override fun addViewControllers(registry: ViewControllerRegistry) {
            registry.addViewController("/").setViewName("forward:/index.html")
        }
        override fun addResourceHandlers(registry: ResourceHandlerRegistry) {
            registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/dist/")
        }
    }
}
package ai.crypto.tracker.auth

import jakarta.servlet.FilterChain
import jakarta.servlet.ServletException
import jakarta.servlet.ServletRequest
import jakarta.servlet.ServletResponse
import jakarta.servlet.http.Cookie
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.Request
import org.springframework.stereotype.Component
import org.springframework.web.filter.GenericFilterBean
import java.io.IOException
import java.util.*


@Component
class AuthenticationFilter : GenericFilterBean() {
    @Throws(IOException::class, ServletException::class)
    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        val httpRequest = request as HttpServletRequest
        val httpResponse = response as HttpServletResponse

        val unprotected = arrayOf(
            "/login",
            "/favicon.ico",
            "/logo.png"
        )

        val path = request.requestURI
        unprotected.forEach { subPath ->
            if (path.startsWith(subPath)) {
                chain.doFilter(request, response)
                return
            }
        }

        val cookies: Array<out Cookie>? = httpRequest.cookies
        var userToken: String? = null
        if (cookies != null) {
            for (cookie in cookies) {
                if (cookie.name.equals("user_token")) {
                    userToken = cookie.value
                    break
                }
            }
        }

        if (userToken.isNullOrEmpty()) {
//            httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Пожалуйста, войдите в систему")
            httpResponse.sendRedirect("/login")
            return
        }

        val url = "http://localhost:8082/user"
        val token = "Bearer $userToken"

        val okHttpClient = OkHttpClient.Builder()
            .protocols(listOf(Protocol.HTTP_1_1))
            .build()

        val checkRequest = Request.Builder()
            .url(url)
            .header("Authorization", token)
            .get()
            .build()

        val checkResponse = okHttpClient.newCall(checkRequest).execute()
        if (checkResponse.code != 200) {
            val cookie = Cookie("user_token", "")
            cookie.path = "/"
            httpResponse.addCookie(cookie)
            httpResponse.sendRedirect("/login")
        }

        chain.doFilter(request, response)
    }
}


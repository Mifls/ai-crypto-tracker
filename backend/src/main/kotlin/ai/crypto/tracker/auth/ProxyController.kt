package ai.crypto.tracker.auth

import io.swagger.v3.oas.annotations.tags.Tag
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.net.URISyntaxException
import java.util.*


@Tag(name = "Login proxy")
@RestController
@RequestMapping("/login")
class ProxyController(private val proxyService: ProxyService) {
    @RequestMapping("/**")
    @Throws(URISyntaxException::class)
    fun sendRequestToSPM(
        @RequestBody(required = false) body: String?,
        method: HttpMethod, request: HttpServletRequest, response: HttpServletResponse
    ): ResponseEntity<ByteArray> {
        return proxyService.processProxyRequest(body, method, request, response, UUID.randomUUID().toString())
    }

}
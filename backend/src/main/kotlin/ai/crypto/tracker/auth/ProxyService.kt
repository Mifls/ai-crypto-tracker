package ai.crypto.tracker.auth;

import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.ThreadContext
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.http.client.BufferingClientHttpRequestFactory
import org.springframework.http.client.ClientHttpRequestFactory
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.stereotype.Service
import org.springframework.web.client.HttpStatusCodeException
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI
import java.net.URISyntaxException


@Service
class ProxyService {

    @Throws(URISyntaxException::class)
    fun processProxyRequest(
        body: String?,
        method: HttpMethod?, request: HttpServletRequest, response: HttpServletResponse?, traceId: String?
    ): ResponseEntity<ByteArray> {
        ThreadContext.put("traceId", traceId)
        var requestUrl = request.requestURI
            .split("/").drop(2).joinToString("/")

        requestUrl = if (requestUrl != "") "/auth/$requestUrl" else "/auth"

        //log if required in this line
        var uri = URI("http", null, "localhost", 8082, null, null, null)

        // replacing context path form urI to match actual gateway URI
        uri = UriComponentsBuilder.fromUri(uri)
            .path(requestUrl)
            .query(request.queryString)
            .build(true).toUri()
        val headers = HttpHeaders()
        val headerNames = request.headerNames
        while (headerNames.hasMoreElements()) {
            val headerName = headerNames.nextElement()
            headers[headerName] = request.getHeader(headerName)
        }
        headers["TRACE"] = traceId
        headers.remove(HttpHeaders.ACCEPT_ENCODING)
        val httpEntity = HttpEntity(body, headers)
        val factory: ClientHttpRequestFactory = BufferingClientHttpRequestFactory(SimpleClientHttpRequestFactory())
        val restTemplate = RestTemplate(factory)
        return try {
            val serverResponse = restTemplate.exchange(
                uri,
                method!!, httpEntity,
                ByteArray::class.java
            )
            logger.info(serverResponse)
            serverResponse
        } catch (e: HttpStatusCodeException) {
            logger.error(e.message)
            ResponseEntity.status(e.rawStatusCode)
                .headers(e.responseHeaders)
                .body(e.responseBodyAsByteArray)
        }
    }

    companion object {
        private val logger = LogManager.getLogger(ProxyService::class.java)
    }
}